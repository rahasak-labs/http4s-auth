scalaVersion := "2.12.6"

version := "1.0"

libraryDependencies ++= {

  lazy val http4sVersion = "0.20.8"
  lazy val circeVersion = "0.9.1"

  Seq(
    "org.http4s"            %% "http4s-blaze-server"    % http4sVersion,
    "org.http4s"            %% "http4s-dsl"             % http4sVersion,
    "org.http4s"            %% "http4s-server"          % http4sVersion,
    "org.http4s"            %% "http4s-core"            % http4sVersion,
    "org.http4s"            %% "http4s-circe"           % http4sVersion,
    "io.circe"              %% "circe-core"             % circeVersion,
    "io.circe"              %% "circe-generic"          % circeVersion
  )

}

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.0")

scalacOptions ++= Seq("-Ypartial-unification")

