package com.rahasak.http4s

import cats.data._
import cats.effect._
import cats.implicits._
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.server.AuthMiddleware
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.util.CaseInsensitiveString

object Main extends IOApp {

  sealed trait Error

  case object AnyError extends Error

  case class User(id: Long, name: String)

  /**
    * Sample function to get auth user from auth header
    *
    * @param authHeader http Authorization header
    * @return auth user
    */
  def getAuthUserFromHeader(authHeader: String): IO[Option[User]] = {
    println(s"get user from auth header $authHeader")
    IO {
      // TODO validate auth header, decode auth header, find user from auth user id(e.g from db)
      Option(User(1, s"auth user for $authHeader"))
    }
  }

  /**
    * Http4s AuthMiddleware function
    *
    * @return Kleisli function
    */
  def authUser: Kleisli[IO, Request[IO], Either[Error, User]] = Kleisli { request: Request[IO] =>
    // get Authorization header
    val header: Option[Header] = request.headers.get(CaseInsensitiveString("Authorization"))
    header match {
      case Some(h) =>
        getAuthUserFromHeader(h.value).map(_.toRight(AnyError))
      case None => IO.pure(Left(AnyError))
    }
  }

  /**
    * Handle authentication failures, returns HTTP 401
    *
    * @return
    */
  def onAuthFailure: AuthedRoutes[Error, IO] = Kleisli { req: AuthedRequest[IO, Error] =>
    // for any requests' auth failure we return 401
    req.req match {
      case _ =>
        OptionT.pure[IO](
          Response[IO](
            status = Status.Unauthorized
          )
        )
    }
  }

  // auth middleware
  def authMiddleware: AuthMiddleware[IO, User] = AuthMiddleware(authUser, onAuthFailure)

  case class Document(id: String, name: String, timestamp: Long)

  /**
    * documents api with AuthedRoutes
    *
    * @return
    */
  def documentRoutes: AuthedRoutes[User, IO] = {
    object dsl extends Http4sDsl[IO] {}
    import dsl._

    AuthedRoutes.of {
      case GET -> Root / "documents" as user =>
        println(s"get documents by $user")

        // TODO find documents from document store

        // return sample docs
        val docs = List(Document("21", "ops", 232323), Document("31", "lambda", 121212))
        Ok(docs)
      case req@POST -> Root / "documents" as user =>
        req.req.decode[Document] { acc =>
          println(s"create document $acc by $user")

          // TODO create document in document store

          Created()
        }
    }
  }

  case class Account(id: String, name: String, balance: Int)

  /**
    * accounts api with HttpRoutes
    *
    * @return
    */
  def accountRoutes: HttpRoutes[IO] = {
    object dsl extends Http4sDsl[IO] {}
    import dsl._
    HttpRoutes.of {
      case GET -> Root / "accounts" =>
        println(s"get accounts")
        // TODO find accounts from account store

        // return sample accounts
        val accs = List(Account("121", "eranga", 4500))
        Ok(accs)
    }
  }

  // combine routes with CombineK function
  def routes: HttpRoutes[IO] = authMiddleware.apply(documentRoutes) <+> accountRoutes

  override def run(args: List[String]): IO[ExitCode] =
    BlazeServerBuilder[IO]
      .bindHttp(8080)
      .withHttpApp(routes.orNotFound)
      .serve
      .compile
      .drain
      .map(_ => ExitCode.Success)

}

