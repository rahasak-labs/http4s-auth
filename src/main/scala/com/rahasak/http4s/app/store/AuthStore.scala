package com.rahasak.http4s.app.store

import cats.effect.IO
import com.rahasak.http4s.Main.User

trait AuthStore {
  def getAuthUser(id: String): IO[Option[User]]
}
