package com.rahasak.http4s.app.store

import cats.effect.IO
import com.rahasak.http4s.Main.User

class AuthStoreImpl extends AuthStore {
  override def getAuthUser(id: String): IO[Option[User]] = {
    println(s"get auth user with header $id")
    IO {
      // TODO validate auth header, find user from auth header(e.g from db)
      Option(User(1, s"auth user for $id"))
    }
  }
}
