package com.rahasak.http4s.app.web

import cats.data._
import cats.effect._
import com.rahasak.http4s.Main.{AnyError, Error, User}
import com.rahasak.http4s.app.store.AuthStore
import org.http4s._
import org.http4s.server.AuthMiddleware
import org.http4s.util.CaseInsensitiveString

object Auth {

  /**
    * Http4s AuthMiddleware function
    *
    * @return Kleisli function
    */
  def authUser(authStore: AuthStore): Kleisli[IO, Request[IO], Either[Error, User]] = Kleisli { request: Request[IO] =>
    val header: Option[Header] = request.headers.get(CaseInsensitiveString("Authorization"))
    header match {
      case Some(h) =>
        authStore.getAuthUser(h.value).map(_.toRight(AnyError))
      case None => IO.pure(Left(AnyError))
    }
  }

  /**
    * Handle authentication failures, returns HTTP 401
    *
    * @return
    */
  def onAuthFailure: AuthedRoutes[Error, IO] = Kleisli { req: AuthedRequest[IO, Error] =>
    // for any requests' auth failure we return 401
    req.req match {
      case _ =>
        OptionT.pure[IO](
          Response[IO](
            status = Status.Unauthorized
          )
        )
    }
  }

  // auth middleware
  def authMiddleware(authStore: AuthStore): AuthMiddleware[IO, User] = AuthMiddleware(authUser(authStore), onAuthFailure)

}


