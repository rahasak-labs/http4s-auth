package com.rahasak.http4s.app.web

import cats.effect._
import cats.implicits._
import com.rahasak.http4s.Main.User
import com.rahasak.http4s.app.store.AuthStore
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl.Http4sDsl

object Route {

  case class Document(id: String, name: String, timestamp: Long)

  /**
    * documents api with AuthedRoutes
    *
    * @return
    */
  def documentRoutes: AuthedRoutes[User, IO] = {
    object dsl extends Http4sDsl[IO] {}
    import dsl._

    AuthedRoutes.of {
      case GET -> Root / "documents" as user =>
        println(s"get documents by $user")

        // TODO find documents from document store

        // return sample docs
        val docs = List(Document("21", "ops", 232323), Document("31", "lambda", 121212))
        Ok(docs)
      case req@POST -> Root / "documents" as user =>
        req.req.decode[Document] { acc =>
          println(s"create document $acc by $user")

          // TODO create document in document store

          Created()
        }
    }
  }

  case class Account(id: String, name: String, balance: Int)

  /**
    * accounts api with HttpRoutes
    *
    * @return
    */
  def accountRoutes: HttpRoutes[IO] = {
    object dsl extends Http4sDsl[IO] {}
    import dsl._
    HttpRoutes.of {
      case GET -> Root / "accounts" =>
        println(s"get accounts")
        // TODO find accounts from account store

        // return sample accounts
        val accs = List(Account("121", "eranga", 4500))
        Ok(accs)
    }
  }

  // combine routes with CombineK function
  def routes(authStore: AuthStore): HttpRoutes[IO] = Auth.authMiddleware(authStore).apply(documentRoutes) <+> accountRoutes

}
