package com.rahasak.http4s.app

import cats.effect._
import com.rahasak.http4s.app.web.Route
import com.rahasak.http4s.app.store.AuthStoreImpl
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder

class AuthApp extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    BlazeServerBuilder[IO]
      .bindHttp(8080)
      .withHttpApp(Route.routes(new AuthStoreImpl).orNotFound)
      .serve
      .compile
      .drain
      .map(_ => ExitCode.Success)

}
